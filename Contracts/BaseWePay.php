<?php

// +----------------------------------------------------------------------
// | WeChatDeveloper
// +----------------------------------------------------------------------
// | 版权所有 2014~2020 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: http://think.ctolog.com
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zoujingli/WeChatDeveloper
// +----------------------------------------------------------------------

namespace unionPay\Contracts;

use think\admin\extend\HttpExtend;
use unionPay\Exceptions\InvalidArgumentException;
use unionPay\Exceptions\InvalidResponseException;

/**
 * 支付宝支付基类
 * Class AliPay
 * @package AliPay\Contracts
 */
abstract class BaseWePay
{

    /**
     * 支持配置
     * @var DataArray
     */
    protected $config;

    /**
     * 当前请求数据
     * @var DataArray
     */
    protected $params;

    /**
     * 静态缓存
     * @var static
     */
    protected static $cache;

    /**
     * 正常请求网关
     * @var string
     */
    protected $gateway = 'https://qr.chinaums.com/netpay-route-server/api/';

    /**
     * AliPay constructor.
     * @param array $options
     */
    public function __construct($data)
    {
        $options = sysconf('unionpay.');
        if ($data)     $options = array_merge($options,$data);

        $this->config = new DataArray($options);
        if (empty($options['mid'])) {
            throw new InvalidArgumentException("Missing Config -- [mid]");
        }
        if (empty($options['tid'])) {
            throw new InvalidArgumentException("Missing Config -- [tid]");
        }
        if (empty($options['instMid'])) {
            throw new InvalidArgumentException("Missing Config -- [instMid]");
        }
        if (empty($options['msgSrc'])) {
            throw new InvalidArgumentException("Missing Config -- [msgSrc]");
        }
        if (empty($options['msgSrcId'])) {
            throw new InvalidArgumentException("Missing Config -- [msgSrcId]");
        }
        if (empty($options['key'])) {
            throw new InvalidArgumentException("Missing Config -- [key]");
        }
        if (!empty($options['debug'])) {
            $this->gateway = 'https://qr-test2.chinaums.com/netpay-route-server/api/';
        }

        $this->params = new DataArray([
            'mid'    => $this->config->get('mid'),
            'tid'    => $this->config->get('tid'),
            'instMid'    => $this->config->get('instMid'),
            'msgSrc'    => $this->config->get('msgSrc'),
            'srcReserve'    => 'zsh',
            'tradeType'    => 'MINI',
            'signType'    => 'SHA256',
            'subAppId'    => 'wxfb608ccc4e8484d3',
            'requestTimestamp'    => date('Y-m-d H:i:s'),
        ]);

    }

    /**
     * 静态创建对象
     * @param array $config
     * @return static
     */
    public static function instance(array $config)
    {
        $key = md5(get_called_class() . serialize($config));
        if (isset(self::$cache[$key])) return self::$cache[$key];
        return self::$cache[$key] = new static($config);
    }

    /**
     * 请求接口并验证访问数据
     * @param $options
     * @return mixed
     * @throws InvalidResponseException
     * @throws \unionpay\Exceptions\LocalCacheException
     */
    protected function getResult($options)   {
        if (isset($options['merOrderId']) && !empty($options['merOrderId']))   $options['merOrderId'] = $this->config->get('msgSrcId') . $options['merOrderId'];
        $this->applyData($options);
        $data = json_decode(Tools::post($this->gateway, json_encode($this->params->get(),JSON_UNESCAPED_UNICODE)), true);
        return $data;
    }

    /**
     * 数据包生成及数据签名
     * @param array $options
     */
    protected function applyData($options)
    {
        foreach ($this->params->merge($options) as $offset => $value)  $this->params->set($offset, $value);
        $this->params->set('sign', $this->getSign());
    }
    /**
     * 获取数据签名
     * @return string
     */
    protected function getSign($data=[],$options=[])  {
        $data = $data ? $data : $this->params->get();
        ksort($data);
        if (isset($data['sign'])) unset($data['sign']);
        foreach ($data as $k => $v) array_push($options,"{$k}={$v}");
        $options = implode('&',$options) . $this->config->get('key');
        return strtoupper(hash('SHA256', $options));
    }
}
