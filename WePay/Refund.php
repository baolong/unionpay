<?php

// +----------------------------------------------------------------------
// | WeChatDeveloper
// +----------------------------------------------------------------------
// | 版权所有 2014~2020 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: http://think.ctolog.com
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zoujingli/WeChatDeveloper
// +----------------------------------------------------------------------

namespace WePay;

namespace unionPay\WePay;

use unionPay\Contracts\BaseWePay;

/**
 * 微信商户退款
 * Class Refund
 * @package WePay
 */
class Refund extends BaseWePay
{

    /**
     * 创建退款订单
     * @param array $options
     * @return mixed
     * @throws \unionPay\Exceptions\InvalidResponseException
     * @throws \unionpay\Exceptions\LocalCacheException
     */
    public function create(array $options)
    {
        $options['msgType'] = 'refund';
        return $this->getResult($options);
    }

    /**
     * 查询退款
     * @param array $options
     * @return mixed
     * @throws \unionPay\Exceptions\InvalidResponseException
     * @throws \unionpay\Exceptions\LocalCacheException
     */
    public function query(array $options)
    {
        $options['msgType'] = 'refundQuery';
        return $this->getResult($options);
    }



}