<?php

// +----------------------------------------------------------------------
// | WeChatDeveloper
// +----------------------------------------------------------------------
// | 版权所有 2014~2020 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: http://think.ctolog.com
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zoujingli/WeChatDeveloper
// +----------------------------------------------------------------------

namespace unionPay\WePay;

use unionPay\Contracts\BaseWePay;
use unionPay\Exceptions\InvalidResponseException;

/**
 * 微信商户订单
 * Class Order
 * @package WePay
 */
class Order extends BaseWePay
{
    /**
     * 统一下单
     * @param array $options
     * @return mixed
     * @throws \unionPay\Exceptions\InvalidResponseException
     * @throws \unionpay\Exceptions\LocalCacheException
     */
    public function create(array $options)
    {
        $options['msgType'] = 'wx.unifiedOrder';
        return $this->getResult($options);
    }

    /**
     * 查询订单
     * @param array $options
     * @return mixed
     * @throws \unionPay\Exceptions\InvalidResponseException
     * @throws \unionpay\Exceptions\LocalCacheException
     */
    public function query(array $options)
    {
        $options['msgType'] = 'query';
        return $this->getResult($options);
    }

    /**
     * 关闭订单
     * @param $outTradeNo
     * @return mixed
     * @throws \unionPay\Exceptions\InvalidResponseException
     * @throws \unionpay\Exceptions\LocalCacheException
     */
    public function close($outTradeNo,$options=[])
    {
        $options['msgType'] = 'close';
        $options['merOrderId'] = $outTradeNo;
        return $this->getResult($options);
    }

    /**
     * 获取微信支付通知
     * @return mixed
     */
    public function getNotify()
    {
        $options = app()->request->post();
        if (isset($options['sign']) && $this->getSign($options) === $options['sign']) {
            $options['orderId'] = str_replace($this->config->get('msgSrcId'),'',$options['merOrderId']);
            return $options;
        }
        throw new InvalidResponseException('Invalid Notify.', '0');
    }

}