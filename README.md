### **一、引言** 

本接口是根据 《银联商务小程序综合支付商户接入接口规范V4.5》接口规范开发而来

###  **二、概述**

本接口定义第三方接入银联商务微信小程序综合支付的接口标准。

###  **三、适用范围**
    
此接口适用第三方接入银联商务微信小程序综合支付的开发。

###  **四、安装说明**

    composer安装：composer require baolong/unionpay:dev-master
    composer卸载：composer remove baolong/unionpay

### **五、功能** 
    
微信小程序支持，【平台下单】【支付请求】【支付结果查询接口】【订单退款接口】【退货查询接口】【订单关闭接口】

###  **一、使用说明**

1. **平台下单**
    ```
    $pay = unionPay::WePayOrder();
    $options = [
        'orderDesc'             => '商品【购买】',
        'merOrderId'     => CodeExtend::uniqidDate(18,'TX'),
        'totalAmount'        => 100,
        'notifyUrl'       => sysuri("pcnsos/api.notify/wxpay", [], false, true),
        'subOpenId'           => "WXsdadasdadadasd"
    ];
    $res = $pay->create($options);
    ```
2. **订单查询**
    ```
   $pay = unionPay::WePayOrder();
    $options = [
        'merOrderId' => '订单编号',
    ];
    $res = $pay->query($options);
    ```
4. **订单关闭**
    ```
    $pay = unionPay::WePayRefund(); 
    $res = $pay->close('订单编号');
    ```
   
3. **订单退款**
    ```
    $pay = unionPay::WePayRefund();
    $options = [
        'merOrderId' => '订单编号',
    ];
    $res = $pay->create($options);
    ```
4. **订单退款查询**
    ```
    $pay = unionPay::WePayRefund();
    $options = [
        'merOrderId' => '订单编号',
    ];
    $res = $pay->query($options);
    ```
