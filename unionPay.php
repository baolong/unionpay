<?php

// +----------------------------------------------------------------------
// | 谱创科技
// +----------------------------------------------------------------------
// | 版权所有 2018~2022 吉林省谱创科技发展有限公司 [ http://www.7dcs.com ]
// +----------------------------------------------------------------------
// | Author: 宝龙  QQ: 117384790  Wechat: bao807735
// +----------------------------------------------------------------------


use unionPay\Contracts\DataArray;
use unionPay\Exceptions\InvalidInstanceException;

/**
 * Class unionPay
 * @package unionPay
 *
 * ----- WePay -----
 * @method unionPay\WePay\Order WePayOrder($options = []) static 微信商户订单
 * @method unionPay\WePay\Refund WePayRefund($options = []) static 微信商户退款
 */
class unionPay {
    /**
     * 定义当前版本
     * @var string
     */
    const VERSION = '1.0.1';

    /**
     * 静态配置
     * @var DataArray
     */
    private static $config;

    /**
     * 设置及获取参数
     * @param array $option
     * @return array
     */
    public static function config($option = null)
    {
        if (is_array($option)) {
            self::$config = new DataArray($option);
        }
        if (self::$config instanceof DataArray) {
            return self::$config->get();
        }
        return [];
    }

    /**
     * 静态魔术加载方法
     * @param string $name 静态类名
     * @param array $arguments 参数集合
     * @return mixed
     * @throws InvalidInstanceException
     */
    public static function __callStatic($name, $arguments)
    {
        if (substr($name, 0, 6) === 'AliPay') {
            $class = 'unionPay\\AliPay\\' . substr($name, 6);
        } elseif (substr($name, 0, 5) === 'WePay') {
            $class = 'unionPay\\WePay\\' . substr($name, 5);
        }

        if (!empty($class) && class_exists($class)) {
            $option = array_shift($arguments);
            $config = is_array($option) ? $option : [];
            return new $class($config);
        }
        throw new InvalidInstanceException("class {$name} not found");
    }
}